########################################################################
# housekeeping
########################################################################

use v6.d;

unit module ProcStats::Handlers:ver<0.3.0>:auth<CPAN:lembark>;

########################################################################
# package variables & constants
########################################################################

# rusage fields returned by posix rusage.

constant POSIX =
<
    maxrss  ixrss   idrss   isrss
    minflt  majflt  nswap
    inblock oublock msgsnd
    msgrcv  nsignals nvcsw   nivcsw
>;

########################################################################
# methods 
########################################################################

sub extract-linux
(
    --> Hash
)
{
    # rusage fields not available on linux.

    constant IGNORE =
    <
        ixrss idrss isrss
        nswap
        msgsnd msgrcv nsignals
    >;

    my $wtime       = now.Num;

    use nqp;
    nqp::getrusage( my int @raw );

    my ( $user_s, $user_us, $syst_s, $syst_us )
    = splice @raw, 0, 4;

    # times are reported as seconds & micro-seconds, 
    # converting them to rationals here works better
    # in Perl6.

    my $utime   = $user_s + $user_us / 1_000_000;
    my $stime   = $syst_s + $syst_us / 1_000_000;

    # simpler to generate a hash and delete the items.

    my %rusage  = POSIX Z=> @raw;
    %rusage{ IGNORE } :delete;

    % =
    (
          |%rusage
        , :$wtime
        , :$utime
        , :$stime
    )
}

sub handler-name
(
    --> Stringy
)
is export( :DEFAULT )
{
    # name lookup simplifies introspection and error
    # reporting.
    #
    # whatever logic is necessary to pick the proper
    # handler goes here...
    #
    # for now this works.

    'extract-' ~ $*KERNEL.name
}

sub choose-handler
(
    --> Code
)
is export( :DEFAULT )
{
    # this is where normal users will enter the cycle:

    my $name    = handler-name;

    &::( $name )
    or
    die "Unkown handler: '$name'"
}

########################################################################
# end
########################################################################
