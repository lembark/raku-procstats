########################################################################
# housekeeping
########################################################################

use v6.d;

unit module ProcStats:ver<0.3.0>:auth<CPAN:lembark>;

########################################################################
# package variables & constants
########################################################################

########################################################################
# HAK ALERT:
# constants here belong in a separate module accessable to the 
# O/S-specific modules. need to get the syntax for exporting 
# a constant correct first, however.
########################################################################

# returned by getrusage

constant FIELDS =
<
    maxrss  ixrss   idrss   isrss
    minflt  majflt  nswap
    inblock oublock msgsnd
    msgrcv  nsignals nvcsw   nivcsw
>;

# wallclock and user time always increase.
# only include them if the other stats are
# non-zero.

constant COMPARE    =
<
    stime
    maxrss
    minflt majflt
    inblock oublock
>;

constant HEADER     =
<
    wtime utime
>;

our &extract-stats
is export( :Extract )
= do
{
    use ProcStats::Handlers;

    choose-handler
};

########################################################################
# subroutines
########################################################################

########################################################################
# in case someone wants to extract their own values use ":Extract".
# roll-your-own gormatting uses ":Diff".
# other than that :Print or :DEFAULT gets the diff-and-format.

sub diff-stats
(
      %sample
    , Bool() :$final = False
    , Bool() :$first = $final
    , Bool() :$force = $final
    --> Hash
)
is export( :Extract )
{
    state %init;
    state %last;

    %init
    or do
    {
        %init = %last = %sample;

        return % = ( |HEADER, |COMPARE ) Z=> ( 0 xx HEADER+COMPARE )
    };

    # note that %prior here is a copy,
    # allowing updating %last
    # w/o getting zero for everything.

    my %prior
    = $first
    ?? %init
    !! %last
    ;

    # note '# Sample: ', %sample.perl;
    # note '# Prior:  ', %prior.perl;

    %last   = %sample;

    # i.e, if the values compared are different then return
    # those along with the header fields. otherwise return
    # nothing.

    my @keys
    =  $force
    ?? COMPARE
    !! COMPARE.grep( { %sample{$_} != %prior{$_} } )
    ;

    ? @keys
    or return %;

    # note '# Keys:   ', @keys.perl;

    % = ( |HEADER, |@keys ) Z=> %prior{ |HEADER, |@keys }
}

sub print-stats
(
        Bool()      :$final = False
    ,   Stringy()   :$label = ''
    ,   *%args
    --> Bool
)
is export( :DEFAULT :Print )
{
    constant WIDTH  = FIELDS>>.chars.max;
    constant MICRO  = 1 / 1_000_000;

    my %sample  = extract-stats;
    my %diff    = diff-stats %sample, :$final, |%args
    or return False;

    # note '# Diff:   ', %diff.perl;

    # if we get this far then there is something to print.
    # write-stat can live anywhere, write-diff is simpler
    # w/ access %diff so it lives here.

    sub write-stat ( Pair $p --> Nil )
    {
        note
        sprintf '%-*s : %s', WIDTH, $p.key, $p.value
    }

    sub write-diff( Pair $p --> Nil )
    {
        my $k   = $p.key;
        my $v   = ( %sample{ $k } - $p.value ).round( MICRO );

        write-stat ( $k => '+' ~ $v )
    }

    note '---';
    write-stat ( :$label                    ) if ? $label;
    write-stat ( Final  => True             ) if ? $final;
    write-stat ( output => $++              );
    write-stat ( sample => %sample<sample>  );

    once
    {
        # avoid leading '+' for the first pass to
        # show that these are baseline numbers.

        for |HEADER, |COMPARE -> $stat
        {
            write-stat ( $stat => %sample{ $stat }  )
        }

        return True
    }

    for %diff.sort -> $stat
    {
        write-diff $stat
    }

    # caller knows sample was output.

    True;
}

########################################################################
# end
########################################################################
